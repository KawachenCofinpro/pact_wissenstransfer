# Was bringt dieses Pact und wie baut man es?

### Grundlegende Idee: "Wir wollen die Schnittstellen absichern!"

- Der Client sagt dem Provider was er haben will und damit sollen die Schnittstellen zusammen passen

## Prinzipien für einen guten Pact Test

### 1. Konkrete Werte

- Jeder Pact muss konkrete Beispielwerte enthalten
- Ansonsten wird bei jedem Durchlauf des Pact Client Tests ein neuer Pact generiert
    - -> Beispiel

### 2. Strict Request Lax Response

- Was ist damit gemeint?
    - Strict Bsp.: StringValue
    - Lax Bsp.: StringType

#### Strict Request
- Type Matching sorgt für einen umfangreicheren Pact
- Sorgt außerdem dafür, dass immer die gleichen Werte im Pact stehen (Konkrete Werte)
    
#### Lax Response
- Ein Provider kann für mehrere Consumer verwendet werden
- Daher sollte die Response so abstrakt wie möglich sein
    - -> Beispiel

### 3. Den Pact so klein wie möglich halten

- Der Client sollte nur die Parameter in den Request schreiben, die er wirklich sendet
- Der Client sollte nur die Parameter in der Response schreiben, die er wirklich konsumiert
- So können wir direkt aus dem Pact lesen, welcher Teil der Api von welchem Client verwendet wird
- Entfernen wir einen Client, kann möglicherweise ein Teil der Api entfernt werden
    - -> Beispiel
    
## Zusammenfassung was bringt uns der Pact

- Schnittstellen werden abgesichert, zur Laufzeit gibt es weniger Fehler
- Abhängigkeiten zwischen den Bausteinen sind im Pact Broker ersichtlich
    - Klärt die Frage: "Welche Consumer müssen wir bei einer Api Änderung anpassen?"
- Die Pacts zeigen uns nicht nur welche Bausteine von einem Provider abhängig sind, sondern auch welchen Teil sie von der Api des Providers nutzen
    - Dadurch können wir unsere Api's von obsoleten Parametern befreien
  
## Jetzt können wir uns noch mal ein paar praktische Beispiele für Pacts in unseren Projekten ansehen ...
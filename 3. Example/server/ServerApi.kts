data class Customer(
    val firstName: String,
    val lastName: String,
    val appSpecificName: String,
)